import asyncio
import socket

async def receive_sdp():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(("127.0.0.1", 0))

    while True:
        data, _ = sock.recvfrom(1024)
        sdp_type = data.decode()
        print(f"Received {sdp_type} SDP")

        if sdp_type == "bye":
            print("Received BYE, terminating...")
            break

async def main():
    while True:
        await receive_sdp()

if __name__ == "__main__":
    asyncio.run(main())
