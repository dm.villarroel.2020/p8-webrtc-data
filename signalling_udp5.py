import socket
import json

UDP_IP = "127.0.0.1"
UDP_PORT = 5005

servers = {}

def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((UDP_IP, UDP_PORT))

    print("Signalling UDP Server running...")

    while True:
        data, addr = sock.recvfrom(1024)
        message = data.decode()
        print(f"Received message: {message} from {addr}")

        if message.startswith("REGISTER"):
            _, server_name = message.split(" ")
            servers[server_name] = addr
            print(f"Registered server: {server_name}")

        elif message == "bye":
            server_name = [name for name, address in servers.items() if address == addr]
            if server_name:
                del servers[server_name[0]]
                print(f"Unregistered server: {server_name[0]}")

        elif message.startswith("OFFER"):
            _, server_name, client_name = message.split(" ")
            server_addr = servers.get(server_name)
            if server_addr:
                sock.sendto(f"{client_name} wants to communicate with you".encode(), server_addr)
                print(f"Forwarding offer to {server_name}")

if __name__ == "__main__":
    main()
