import asyncio
import socket
import sys

async def receive_sdp(server_name):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(("127.0.0.1", 0))  # Bind to any available port

    signalling = ("127.0.0.1", 5005)
    sock.sendto(f"REGISTER {server_name}".encode(), signalling)
    print(f"Registered with server: {server_name}")

    while True:
        data, _ = sock.recvfrom(1024)
        message = data.decode()
        print(f"Received message: {message}")

        if message == "bye":
            print("Received BYE, terminating...")
            break

async def main():
    if len(sys.argv) != 2:
        print("Usage: python client_server_udp5.py <server_name>")
        return

    server_name = sys.argv[1]
    await receive_sdp(server_name)

if __name__ == "__main__":
    asyncio.run(main())
