import asyncio
import json
import socket
import sys

async def send_sdp(signalling, server_name, sdp_type):
    await asyncio.sleep(2)  # Simulating SDP generation
    sdp = {"type": sdp_type, "sdp": "Your SDP message here"}
    sdp_json = json.dumps(sdp)

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    message = f"OFFER {server_name} CLIENT"
    sock.sendto(message.encode(), signalling)
    print(f"Sent {sdp_type} SDP to {server_name}")

async def main():
    if len(sys.argv) != 3:
        print("Usage: python client_data_udp5.py <server_name>")
        return

    server_name = sys.argv[1]
    signalling = ("127.0.0.1", 5005)

    await send_sdp(signalling, server_name, "offer")
    await send_sdp(signalling, server_name, "answer")

    # Sending BYE after twelve exchanges
    await asyncio.sleep(2)  # Wait for responses
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto("bye".encode(), signalling)
    print("Sent BYE")

if __name__ == "__main__":
    asyncio.run(main())
