import asyncio
import json
import socket

async def send_sdp(signalling, sdp_type):
    await asyncio.sleep(2)
    sdp = {"type": sdp_type, "sdp": "Your SDP message here"}
    sdp_json = json.dumps(sdp)

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(sdp_type.encode(), signalling)
    print(f"Sent {sdp_type} SDP")

async def main():
    signalling = ("127.0.0.1", 5005)

    await send_sdp(signalling, "offer")
    await send_sdp(signalling, "answer")
    await send_sdp(signalling, "offer")

    # Sending BYE after three exchanges
    await asyncio.sleep(2)  # Wait for responses
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto("bye".encode(), signalling)
    print("Sent BYE")

if __name__ == "__main__":
    asyncio.run(main())
