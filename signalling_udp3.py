import socket
import json

UDP_IP = "127.0.0.1"
UDP_PORT = 5005

def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((UDP_IP, UDP_PORT))

    clients = {}

    print("Signalling UDP Server running...")

    while True:
        data, addr = sock.recvfrom(1024)
        message = data.decode()
        print(f"Received message: {message} from {addr}")

        if message.startswith("REGISTER"):
            _, role = message.split(" ")
            clients[role] = addr

        elif message in ["offer", "answer"]:
            other_role = "SERVER" if message == "offer" else "CLIENT"
            other_addr = clients[other_role]

            if other_addr:
                sock.sendto(data, other_addr)
                print(f"Transmitting {message} to {other_role}")

        elif message == "bye":
            other_role = "CLIENT" if addr == clients["CLIENT"] else "SERVER"
            other_addr = clients[other_role]

            if other_addr:
                sock.sendto(b'bye', other_addr)
                print(f"Transmitting BYE to {other_role}")

if __name__ == "__main__":
    main()
